<?php
    session_start();
    include("includes/Formatting.php");
    heading("Login");

?>
	<!----------------------->

    <?php
    $Err = "";
    $username = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST")   //user has submitted form
    {
        $Err = "Invalid Credentials";   //Assumes credentials are wrong to avoid false positive
        $username = $_POST["Username"];
        
        include("includes/Active.php");
        if(validate_user($username,$_POST["Password"]))
        {
            $_SESSION["username"]=$_POST["Username"];
            $_SESSION["authenticated"] = true;
            header("Location:" . $_GET["redirect"]);
        }
    }

    ?>

    <!---------Body----------->
    <h1>Change Password</h1>
    <h3 style="top: 470;" >
        Login
    </h3>
    
    <form method="post" action=""> 
        <span class="error" style="top:545; position:absolute;"><?php echo $Err;?></span>
        <input class=ResponseBox style="top: 570;" type="text" autocomplete="off" name="Username" placeholder="Username" value=<?php echo $username ?> >
        
        <input class=ResponseBox style="top: 600;" type="password" name="Password" placeholder="Password">
    
        <input style="top:650;" type="submit" name="formSubmit" value="Submit">
    </form>
    <!-------------------------->
    
<?php footer(750, true);?>