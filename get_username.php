<?php
    session_start();
    include("includes/Formatting.php");
    heading("Get Username");
?>
	<!----------------------->

<?php                       //handles Error display or Redirect if required
    $usernameErr="";
    $username="";
    if ($_SERVER["REQUEST_METHOD"] == "POST")   //user has submitted form
    {
        include("includes/Database.php"); 
        
    //Check if username exists
        if(isset($_POST["Username"]))             //user has entered username in form
        {
            $usernameResult = get_SQL_values("username", "username", $_POST["Username"],"users");    
            if($usernameResult=="")   //no result, username is not in database
            {
                $usernameErr="Invalid Username";
                $username=$_POST["Username"];
            }else                               //username is in database
            {
                $_SESSION["username"]=$_POST["Username"];
                header("Location: reset_password.php?");   
            }
        }
    }
    ?>

    <!---------Body----------->
    <h1>Password Reset</h1>
    <h3 style="top: 475;">
        Username
    </h3>
    
    <form method="post" action=""> >
    
        <input class=ResponseBox style="top: 570;" type="text" autocomplete="off" name="Username" placeholder="Username" value=<?php echo $username?> >
        <span class="error" style="top:590; position:absolute;"><?php echo $usernameErr;?></span>
        
        <input style="top:650;" type="submit" name="formSubmit" value="Submit">
    </form>
    <!-------------------------->
    
<?php footer(750, true);?>