<?php
function heading($title) //title of the page
{
    echo "<html>\n";
    if(!isset($title)){
        $title="Password Manager";
    }
    echo "<head>\n";
        echo "\t<title>" . $title . " </title> \n"; 
        echo "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"McQuaid.css\"> \n";
    echo "</head>\n";
    echo "\n";
    echo "<body>\n";
        echo "\t<!--McQuaid Jesuit Header-->\n";
        echo "\t<div class=topblack>\n";
            echo "\t\t<img src=\"images/logo.png\">\n";
        echo "\t</div>\n";
        echo "\t<img class=header src=\"images/band.png\">\n";
        echo "\t<img class= crest src=\"images/crest.png\">\n";
        echo "\t<!----------------------->\n";
}

function footer($height, $home_button) //location of the start of the footer and whether to include the home button
{
    echo "\t<!---Footer----------------->\n";
    if($home_button)
    {
        echo "\t<form action=\"index.php\"> \n";
            echo "\t\t<button style=\"top:" . $height . ";\">Home</button>\n";
        echo "\t</form>\n";
        echo "\n";
        $height+=60;
    }
	       echo "\t<div style=\"top:" . $height . ";\" class=footer>\n";
            echo "\t\t<img src=\"images/logo_footer.png?\">\n";
        echo "\t</div>\n";
        echo "\t<!--------------------------->\n";
    echo "</body>\n";
    echo "</html>\n";
}
?>