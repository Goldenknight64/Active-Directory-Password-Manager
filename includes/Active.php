<?php
// LDAP Constants
define("LDAP_SERVER","DC01");
define("LDAP_ADMIN_USER","mcrockett@knight.mcquaid.org"); //"tborrelli@knight.mcquaid.org");
define("LDAP_ADMIN_PASS","Password1");
define("LDAP_DOMAIN","knight.mcquaid.org");
define("LDAP_BASE_DN","DC=knight,DC=mcquaid,DC=org");
define("LDAP_PSWD_MIN_LENGTH",7);
define("LDAP_PSWD_MAX_LENGTH",255);

function get_min_length()
{
    return LDAP_PSWD_MIN_LENGTH;
}

function get_max_length()
{
    return LDAP_PSWD_MAX_LENGTH;
}

function get_user_displayname($username)
{
    $value = 'displayname';

    $conn = ldap_connect(LDAP_SERVER);
    ldap_set_option( $conn, LDAP_OPT_PROTOCOL_VERSION, 3 );
    $bind = ldap_bind($conn,LDAP_ADMIN_USER,LDAP_ADMIN_PASS);

    $filter = "(samaccountname=" . $username .")";
    $attr = Array($value);

    $result = ldap_search($conn,LDAP_BASE_DN,$filter,$attr);

    $entries = ldap_get_entries($conn,$result);
    return $entries['0'][$value]['0'];
    
}

function validate_user($username, $password)
{
    $username.="@". LDAP_DOMAIN;
    if ($username != "" && $password != "") 
    {
        $conn = ldap_connect(LDAP_SERVER);
        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3 );  
        
        $bind = @ldap_bind($conn,$username,$password);
        return $bind;
    }
}

function is_valid_password($password)
{
    $categories_met = 0;   //must meet at three category of characters (lowercase, upercase, number, special)
    $is_valid = false;
    if( strlen($password)<LDAP_PSWD_MIN_LENGTH || strlen($password)>LDAP_PSWD_MAX_LENGTH)    //Active Directory password length restriction
    {
        return false;
    }
    if(preg_match("@[a-z]@",$password))       //Matches lower case character category
    {
        $categories_met += 1;
    }
    if(preg_match("@[A-Z]@",$password))       //Matches upper case character
    {
        $categories_met += 1;
    }
    if(preg_match("@[0-9]@",$password))       //Matches upper case character
    {
        $categories_met += 1;
    }    
    if(preg_match('/[^A-Za-z0-9\-]/',$password))       //Matches special character
    {
        $categories_met += 1;
    }
    
    if($categories_met>=3)
    {
        $is_valid = true;
    }
    return $is_valid;
}

function set_password($username, $password)
{    
    $success = false;
    if(is_valid_password($password))
    {
        $dn = "CN=" . get_user_displayname($username) . ",OU=Staff," . LDAP_BASE_DN;        //Get the fully qualified domain name
    
        //Setup connection and bind to server
        $conn = ldap_connect("ldaps://" . LDAP_SERVER, 636);            //secure ldap connection
        ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3 );
        $bind = ldap_bind($conn, LDAP_ADMIN_USER,LDAP_ADMIN_PASS);

        //Black magic I found online
        //Formats the password
        $newPassword = $password;
        $newPassword = "\"" . $newPassword . "\"";
        $len = strlen($newPassword);
        $newPassw="";
        for ($i = 0; $i < $len; $i++)
            $newPassw .= "{$newPassword{$i}}\000";
        $newPassword = $newPassw;

        //ldap mod replace
        $userdata["unicodePwd"] = $newPassword;
        $result = ldap_mod_replace($conn, $dn, $userdata);

        //Shows whether it worked
        if ($result)
        {
            $success = true;
        }
    }
    return $success;
}
?>