<?php
    define("SQL_USER","root");
    define("SQL_HOST","localhost");
    define("SQL_PASSWORD","");
    define("SQL_DATABASE","password_manager");

    function get_SQL_values($result, $test_variable, $test_value, $table)
    {
        /******************************************************************************************
        * Author     :          Tyler Borrelli                                                    *
        * Description:          Uses prepared statements to prevent SQL injections in searches    *
        * Return     :          Returns an entry from a SQL table                                 *
        * Variables  :                                                                            *
        *                $result        = the column name of the value to return                  *
        *                $test_variable = the column name to test                                 *
        *                $test_value    = the value to compare against column name in the search  *
        *                $table         = table to execute search in                              *
        ******************************************************************************************/
        $cxn = mysqli_connect(SQL_HOST,SQL_USER,SQL_PASSWORD,SQL_DATABASE) or die ("Couldn't connect to server.");
        if ($statement = $cxn->prepare("SELECT $result FROM $table WHERE $test_variable=?"))
        {
            // Bind a variable to the parameter as a string. 
            $statement->bind_param("s", $test_value);

            // Execute the statement.
            $statement->execute();

            // Get the variables from the query.    
            $statement->bind_result($return);

            // Fetch the data.
            $statement->fetch();

            // Close the prepared statement.
            $statement->close();

            //return the desired varialbe
            return $return;
        }
    }

    function update_SQL_values($update_column, $update_value, $test_variable, $test_value, $table)
    {
        /******************************************************************************************
        * Author     :          Tyler Borrelli                                                    *
        * Description:          Uses prepared statements to prevent SQL injections in updates     *
        * Return     :          Returns an entry from a SQL table                                 *
        * Variables  :                                                                            *
        *                $update_column   = the column name of the value to update                *
        *                $update_value    = value to put into column                              *
        *                $test_variable   = the column name to test                               *
        *                $test_value      = the value to compare against column name in the search*
        *                $table           = table to execute search in                            *
        ******************************************************************************************/

        $cxn = mysqli_connect(SQL_HOST,SQL_USER,SQL_PASSWORD,SQL_DATABASE) or die ("Couldn't connect to server.");

        if ($stmt = $cxn->prepare("UPDATE $table SET $update_column = ? WHERE $test_variable = ?")) 
        {
            // Bind the variables to the parameter as strings. 
            $stmt->bind_param("ss", $update_value, $test_value);

            // Execute the statement.
            $stmt->execute();

            // Close the prepared statement.
            $stmt->close();
        }
    }
?>