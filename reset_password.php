<?php
    session_start();
    include("includes/Formatting.php");
    heading("Security Questions");
    include("includes/Database.php");
?>
	<!----------------------->

<?php                       //Checking information
    //user has not entered username
    if(!isset($_SESSION["username"]))
    {
        header("Location: get_username.php?"); //redirect to have user enter username
        exit;
    }

    //Display Security Question
          $security_question = get_SQL_values("security_question", "username", $_SESSION["username"], "users");
          echo "<p style=\"top:615; position:absolute; left:50%; font-size:20; transform:translate(-50%)\">$security_question</p>";

    $Err="";
    $ID="";
    $response="";

    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        $Err="Invalid Information";         //Assume wrong before checking to avoid false positive match
        $ID=$_POST["Id"];
        $response=$_POST["SecurityAnswer"];

    //Check ID and Security Response
        $security_answer = get_SQL_values("security_answer", "username", $_SESSION["username"],"users");
        $id = get_SQL_values("id", "username", $_SESSION["username"],"users");
        if($_POST["Id"]==$id && $_POST["SecurityAnswer"]==$security_answer)   //Information is right
        {
            $_SESSION["authenticated"]=true;
            header("Location: set_password.php?");
            exit;
        }
    }
    ?>

    <!---------Body----------->
    <h1>Password Reset</h1>
    <h3 style="top:425";>
        ID Number
    </h3>
    
    <h3 style="top:550;">
        Security Question
    </h3>

    <form method="post" action="">
        <span class="error" style="top:435; position:absolute;"><?php echo $Err;?></span>
        
        <input class=ResponseBox style="top: 515;" type="text" autocomplete="off" name="Id" placeholder="ID Number" value=<?php echo $ID?> >
        
        <input class=ResponseBox style="top: 700;" type="text" autocomplete="off" name="SecurityAnswer" placeholder="Response" value=<?php echo $response?>>
        
        <input style="top:750;" type="submit" name="formSubmit" value="Submit">
    </form>

<?php footer(835,true);?>
