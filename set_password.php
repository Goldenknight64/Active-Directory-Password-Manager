<?php
    session_start();
    include("includes/Formatting.php");
    heading("Set Password");
?>
	<!----------------------->

<?php
    if (!$_SESSION["authenticated"] || !isset($_SESSION["authenticated"]) || !isset($_SESSION["username"]))
    {
        header("Location:index.php");
    }

    $Err = "";
    include("includes/Active.php");

    if ($_SERVER["REQUEST_METHOD"] == "POST")   //user has submitted form
    {
        $max = get_max_length();
        $min = get_min_length();
        if($_POST["EnterPassword"]==$_POST["ConfirmPassword"] && $_POST["EnterPassword"]=="") //Both passwords are blank
        {
            $Err = "Passwords are blank";
        }
        elseif($_POST["EnterPassword"]!=$_POST["ConfirmPassword"])      //Passwords don't match
        {
            $Err = "Passwords do not match";
        }
        elseif(strlen($_POST["EnterPassword"])<=$min)   //password does not meet minimum length
        {
            $Err = "Password must be at least $min characters";
        }
        elseif(strlen($_POST["EnterPassword"])>=$max)    //password does not meet maximum length
        {
            $Err = "Password cannot be greater than $max characters";
        }
        elseif(!is_valid_password($_POST["EnterPassword"]))     //Password does not meet active directory requirements
        {
            $Err = "Password does not meet password requirements";
        }
        else
        {
            if( set_password($_SESSION["username"],$_POST["EnterPassword"]) ) //Password set was successful
            {
                header("Location:succesful_reset.php?");
            }
            else
            {
                $Err = "Reset was unsuccessful, please try again later";
            }
        }
    }

?>
    <!---------Body----------->
    <h1>Password Reset</h1>
    <h3 style="top:475";>
        Enter Password
    </h3> 
    <form method="post" action=""> 
        <span class="error" style="top:545; position:absolute;"><?php echo $Err;?></span>
        
        <input class=ResponseBox style="top: 600;" type="password" name="EnterPassword" placeholder="Please Enter Password">

        <input class=ResponseBox style="top: 630;" type="password" name="ConfirmPassword" placeholder="Please Confirm Password">
        
        <input style="top:675;" type="submit" name="formSubmit" value="Submit">
    </form>
    
<?php footer(775,true);?>